{-==== ФАМИЛИЯ ИМЯ, НОМЕР ГРУППЫ ====-}
{-==== Светликова Тансу, 11-302 ====-}
 
module Trouble
       ( rocket
 
   Все деления производить нацело.
 -}
rocket :: [(Integer, Integer)] -> [(Integer, Integer)]
rocket xs = error "Design rocket!"
minimum' [x] = x 
minimum' (x:xs) = min' x (minimum' xs) 
max' (x1,x2) (y1,y2) = if ((fromIntegral x1)/(fromIntegral x2)) >= ((fromIntegral y1)/(fromIntegral y2)) then (x1,x2) else (y1,y2) 
maximum' [x] = x 
maximum' (x:xs) = max' x (maximum' xs) 
rocket :: [(Integer, Integer)] -> [(Integer, Integer)] 
rocket xs = if (snd (minimum' xs)) == (snd (maximum' xs)) then [maximum' xs] else xs
 
{- Космический корабль (КК) может состоять из:
 * ракеты, запускающей на заданную высоту несколько КК
                 | Cargo [Load a]           -- полезная нагрузка
                 deriving (Eq,Show)

-}				 
{- Дан список КК

   Вернуть список всех орбитальных аппаратов
λ> orbiters [Rocket 300 [Rocket 200 [Cargo [Orbiter "LRO", Probe "Lunokhod"]], Cargo [Orbiter "ISS"]]]
[Orbiter "ISS", Orbiter "LRO"]
 -}
findApparat :: [Load a] -> [Load a]
findApparat [] = []
findApparat ((Orbiter x):xs) = (Orbiter x):(findApparat xs)
findApparat ((Probe x):xs) = findApparat xs
 
orbiters :: [Spaceship a] -> [Load a]
orbiters = error "Count orbiters!"

orbiters [] = []
orbiters ((Cargo x):xs) = (orbiters xs) ++ (findApparat x)
orbiters ((Rocket _ x):xs) = (orbiters xs) ++ (orbiters x)
{- Даны фразы, нужно выдать того, кто сказал её

   instance Eq - не задан!
λ> finalFrontier [IsDead "He", Fascinating, Warp 3, LiveLongAndProsper]
["McCoy", "Spock", "Kirk", "Spock"]
-}
finalFrontier :: [Phrase] -> [String]
finalFrontier = error "Space, the final frontier. These are the voyages of the Starship Enterprise. Its five-year mission: to explore strange new worlds, to seek out new life and new civilizations, to boldly go where no man has gone before."
lineConcl :: [Phrase] -> [String]
lineConcl [] = []
lineConcl ((Warp _):xs) = "Kirk":(lineConcl xs)
lineConcl ((BeamUp _):xs) = "Kirk":(lineConcl xs)
lineConcl ((IsDead _):xs) = "McCoy":(lineConcl xs)
lineConcl ((LiveLongAndProsper):xs) = "Spock":(lineConcl xs)
lineConcl ((Fascinating):xs) = "Spock":(lineConcl xs)
