ans = sum [i | i <- [2..1000000], i == (eq' i 0)] 
eq' 0 sum' = sum'
eq' x sum' = eq' (div x 10) (sum' + ((mod x 10)^5))