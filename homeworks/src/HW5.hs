-- 2016-04-05 / 2016-04-12


module HW5
       ( Parser (..)
       , dyckLanguage
       , Arith (..)
       , arith
       , Optional (..)
       ) where

import Control.Applicative hiding ( (<|>), many )
{--==========  PARSER ==========-}

type Err = String
data Parser a = Parser
                { parse :: String ->
                  Either Err (a, String) }

instance Functor Parser where
  fmap f p = Parser $ \s -> case parse p s of
    Left err -> Left err
    Right (x, rest) -> Right (f x, rest)

instance Applicative Parser where
dyckLanguage = undefined

data Arith = Plus Arith Arith
           | Minus Arith Arith
           | Mul Arith Arith
           | Number Int
           deriving (Eq,Show)

-- ((123+4321)*(321-3123))+(123+321)
{- Add (Mul (Add (Const 123) (Const 4321))
            (Sub (Const 321) (Const 3213)))
       (Add (Const 123) (Const 321))
-}

arith :: Parser Arith
arith = undefined
arith = plus <|> minus <|> expression

plus  = (\a _ b -> Plus  a b) <$> expression <*> char '+' <*> arith
minus = (\a _ b -> Minus a b) <$> expression <*> char '-' <*> arith
expression = mul <|> num
mul = (\a _ b -> Mul a b) <$> num <*> char '*' <*> expression
num = ((\_ e _ -> e ) <$> char '(' <*> arith <*> char ')' ) <|> (Number <$> number)

-- Инстансы функтора и аппликативного функтора

data Optional a = NoParam
                | Param a
                deriving (Eq,Show)

instance Functor Optional where
  fmap = undefined
  fmap f o = case o of
    NoParam -> NoParam
    Param a -> Param (f a)

instance Applicative Optional where
  pure = undefined
  (<*>) = undefined

  pure a = Param a
  (<*>) pf pa = case pf of
    NoParam -> NoParam
    Param f -> case pa of
      NoParam -> NoParam
      Param a -> Param (f a)