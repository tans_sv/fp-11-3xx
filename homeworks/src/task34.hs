--145 is a curious number, as 1! + 4! + 5! = 1 + 24 + 120 = 145.
--Find the sum of all numbers which are equal to the sum of the factorial of their digits.
--Note: as 1! = 1 and 2! = 2 are not sums they are not included.

factorial :: Int -> Int
factorial 0 = 1
factorial 1 = 1
factorial n = n*factorial(n-1)

digits :: Int -> [Int]
digits 0 = []
digits n = r : digits q
    where (q, r) = quotRem n 10

curious :: Int -> Bool
curious n = n == sum (map factorial (digits n))

main :: IO ()
main = print $ sum $ filter curious [10..50000]