-- Срок: 2016-04-02 (100%), 2016-04-07 (50%)

module HW4
       ( Show' (..)
       , A(..)
       , C(..)
       , Set(..)
       , symmetricDifference
       , fromBool
       , fromInt
       ) where

class Show' a where
  show' :: a -> String

data A = A Int
       | B
-- show' (A 5)    == "A 5"
-- show' (A (-5)) == "A (-5)"
-- show' B        == "B"

data C = C Int Int
-- show' (C 1 2)       == "C 1 2"
-- show' (C (-1) (-2)) == "C (-1) (-2)"

instance Show' A where
  show' = undefined

instance Show' C where
  show' = undefined

instance Show' A where
  show' B = "B"
  show' (A a) = "A " ++ show a
  
instance Show' C where
	show' (C a b) = "C " ++ (if (a<0) then "(" ++ show a ++ ") " else show a ++ " ") 
		++ (if (b<0) then "(" ++ show b ++ ")" else show b)

----------------

data Set a = Set (a -> Bool)

-- Симметрическая разность -
-- элементы обоих множеств, не входящие в объединение
-- {4,5,6} \/ {5,6,7,8} == {4,7,8}
symmetricDifference :: Set a -> Set a -> Set a
symmetricDifference = undefined

symmetricDifference (Set a) (Set b) = Set $ \elem -> let e_a = a elem
                                                         e_b = b elem
                      in (e_a || e_b) && not (e_a && e_b)
-----------------

tru = \t -> (\f -> t)
fls = \t f -> f

-- fromBool - переводит булевское значение в кодировку Чёрча
fromBool = undefined
fromBool True = tru
fromBool False = fls

-- fromInt - переводит число в кодировку Чёрча
fromInt = undefined

fromInt n
    | n == 0 = \s z -> z
    | otherwise = \s z -> s (fromInt (n - 1) s z)