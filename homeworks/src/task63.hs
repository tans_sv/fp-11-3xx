--The 5-digit number, 16807=7^516807=7^5, is also a fifth power. Similarly, the 9-digit number, 134217728=8^9134217728=8^9, is a ninth power.
--How many n-digit positive integers exist which are also an nth power?
ans = print $ sum $ [1 | i <- [1..99], n <- [1..99], length (show (i^n)) == n]