module Sem where

--это терм
data Term' = Variable String
        | Lambda Term' Term'
        | App Term' Term'
        deriving (Eq, Show)

--переименовываем одну переменную
renameVar :: [String] -> String -> String
renameVar listVars var
        |var `elem` listVars = rename
 listVars ((\x -> x ++ x) var)
        |otherwise = var 

--заменяем все одноименные переменные 
renameSameVars :: [String] -> Term' -> Term'
renameSameVars vars (Lambda (Variable x) t) = if x `elem` vars then Lambda (Variable x) (renameSameVars (check vars) t) else (Lambda (Variable x) (renameSameVars vars t))
         where check v = filter (\el -> el /= x) v
renameSameVars vars (App t1 t2) = App (renameSameVars vars t1) (renameSameVars vars t2)
renameSameVars vars (Variable x) = if x `elem` vars then Variable (renameVar vars x) else Variable x 
        

--заменяет нужную переменную var
--и сохраняет все связные переменные 
reduct :: [String] -> String -> Term' -> Term' -> Term' 
reduct vars var (Lambda (Variable x) t) newTerm = if x /= var then (Lambda (Variable x) (reduct (x:vars) var t newTerm)) else (Lambda (Variable x) t)
reduct vars var (App t1 t2) newTerm = App (reduct vars var t1 newTerm) (reduct vars var t2 newTerm)
reduct vars var (Variable x) newTerm = if x == var then renameSameVars vars newTerm else Variable x

-- один шаг
eval1 :: Term' -> Maybe Term' 
eval1 (Lambda (App t1 t2) t) = Nothing 
eval1 (Lambda (Lambda t1 t2) t) = Nothing
eval1 (App (Lambda (Variable x) t1) t) = Just (reduct [] x t1 t)
eval1 (App (App t3 t4) t2) = Just (App evalthis t2)
              where (Just evalthis) = eval1 (App t3 t4)
eval1 (App t1 (App t3 t4)) = Just (App t1 evalthis)
              where (Just evalthis) = eval1 (App t3 t4)
eval1 t = Just t

--несколько шагов
evalN :: Term' -> Term'
evalN (App (Lambda (Variable x) t1) t2) = evalN (reduct [] x t1 t2)
evalN (App (App t1 t2) t) = evalN (App (evalN (App t1 t2)) t)
evalN t = t